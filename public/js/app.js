var app = angular.module('flattr', ['ngRoute']);

app.config(['$locationProvider', '$routeProvider',
    function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
            .when('/', {
                template: '<home-page></home-page>'
            })
            .when('/search/:query', {
                template: '<search-page></search-page>'
            })
            .when('/flattred', {
                template: '<flattred-page></flattred-page>'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);
