app.service('$auth', function ($http) {
    var me = this;

    me.checkLogin = function () {
        return $http.get('/auth/check-login');
    };

    me.signOut = function () {
        return $http.get('/auth/sign-out');
    };
});
