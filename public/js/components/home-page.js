app.component('homePage', {
    template: `
        <video class="home-video" preload muted autoplay loop>
		    <source src="../vid/722783965.mp4" type="video/mp4" />
		</video>
        <div class="jumbotron">
            <h1>Hello, stranger ;)</h1>
            <p>Please register at <a href="https://flattr.com" target="_blank">
                <strong>Flattr.com</strong></a> to use this app</p>
            <p><a href="https://flattr.com" target="_blank" class="btn btn-success btn-lg">Go to 
                <strong>Flattr.com</strong></a></p>
        </div>
    `
});
