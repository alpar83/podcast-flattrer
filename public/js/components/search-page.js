app.component('searchPage', {
    template: `
        <div ng-repeat="podcast in $ctrl.podcasts" class="podcast row">
            <div class="podcast-img col-sm-3">
                <a ng-href="{{podcast.trackViewUrl}}" target="_blank">
                    <img ng-src="{{podcast.artworkUrl600}}" class="img-responsive">
                </a>
            </div>
            
            <div class="podcast-text col-sm-9">
                <div class="row col-md-12">
                    <a ng-href="{{podcast.trackViewUrl}}" target="_blank">
                        <h2 class="truncate" title="{{podcast.trackName}}">{{podcast.trackName}}</h2>
                    </a>
                </div>
                <div class="row col-md-12" style="margin-bottom: 15px;">
                    <a ng-href="{{podcast.artistViewUrl}}" target="_blank">
                        <h4 class="truncate" title="{{podcast.artistName}}"><small>By:</small> {{podcast.artistName}}</h4>
                    </a>
                </div>
                <div class="row">
                    <div class="col-md-2 col-lg-2"><small>Collection:</small></div>
                    <div class="col-md-5 col-lg-10 truncate">
                        <a ng-href="{{podcast.collectionViewUrl}}" target="_blank">
                            <strong title="{{podcast.collectionName}}">{{podcast.collectionName}}</strong>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"><small>Date:</small></div>
                    <div class="col-md-5"><strong>{{podcast.releaseDate | date : "MMM d, y"}}</strong></div>
                </div>
                <div class="row">
                    <div class="col-md-2"><small>Genre:</small></div>
                    <div class="col-md-5"><strong>{{podcast.primaryGenreName}}</strong></div>
                </div>
                <div class="row">
                    <div class="col-md-2"><small>Price:</small></div>
                    <div class="col-md-5"><strong>{{podcast.currency}} {{podcast.trackPrice}}</strong></div>
                </div>
            </div>
            
            <button ng-click="$ctrl.addPodcast(podcast)" class="btn btn-success btn-outline pull-right"
                    style="position: absolute; right: 30px; bottom: 30px;">
                <strong>Flattr</strong> this
            </button>
        </div>
        <footer class="text-center">
            <button ng-click="$ctrl.loadMore()" class="btn btn-success">Load more</button>
        </footer>
    `,
    controller: function ($http, $routeParams) {
        var me = this;

        $http.get('https://itunes.apple.com/search?term=' + $routeParams.query + '&media=podcast')
            .then(function (res) {
                me.podcasts = res.data.results;
            }, function (error) {
                console.error(error);
            });

        me.addPodcast = function (podcast) {
            $http.post('/flattr/add-podcast', {podcast: podcast})
                .then(function (res) {
                    alert(res.data);
                }, function (err) {
                    alert(err.data);
                });
        };

        me.loadMore = function () {
            console.log('loadMore');
        };
    }
});
