app.component('navbar', {
    template: `
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#/">
                        <span style="font-weight: 300;">Podcast</span>
                        <span style="font-weight: 800;">Flattr</span>er
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbar">
                    <form class="navbar-form navbar-left">
                        <input ng-model="$ctrl.query" ng-change="$ctrl.search()" type="text" class="form-control search-box"
                               placeholder="Search..." style="width: 100%;">
                    </form>
                    <!--todo simplify-->
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#/flattred" style="color: #000;">
                                <span style="font-weight: 300;">My</span>
                                <span style="font-weight: 800;">Flattr</span>ed
                            </a>
                        </li>
                        <li>
                            <button ng-show="!$ctrl.isSignedIn" ng-click="$ctrl.signIn()" 
                                    type="button" class="btn btn-success navbar-btn">
                                Sign in
                            </button>
                            <button ng-show="$ctrl.isSignedIn" ng-click="$ctrl.signOut()" 
                                    type="button" class="btn btn-success navbar-btn">
                                Sign out
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    `,
    controller: function ($routeParams, $rootScope, $auth, $location) {
        var me = this;

        me.query = $routeParams.query;

        $rootScope.$on('$routeChangeSuccess', function () {
            me.query = $routeParams.query;
        });

        $auth.checkLogin()
            .then(function (res) {
                me.isSignedIn = res.data;
            }, function (err) {
                alert(err.data);
            });

        me.search = function () {
            $location.path('/search/' + me.query.toLowerCase());
        };

        me.signIn = function () {
            // window.location = 'https://flattr.com/oauth/authorize?client_id=kGiu9M9s6uAaiE9X9AHvRm8iwCsUcWxA&response_type=code&redirect_uri=http://localhost:3000/auth/?from=' + $location.path() + '&scope=flattr';
            window.location = 'https://flattr.com/oauth/authorize?client_id=kGiu9M9s6uAaiE9X9AHvRm8iwCsUcWxA&response_type=code&redirect_uri=http://podcast-flattrer.herokuapp.com/auth/?from=' + $location.path() + '&scope=flattr';
        };

        me.signOut = function () {
            $auth.signOut()
                .then(function (res) {
                    alert(res.data);
                    me.isSignedIn = false;
                    $location.path('/');
                }, function (err) {
                    alert(err.data);
                });
        };
    }
});
