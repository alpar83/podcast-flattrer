app.component('flattredPage', {
    template: `
        <div ng-show="$ctrl.err" class="alert alert-danger center-block"><strong>Error:</strong> {{$ctrl.err.data}}</div>
        <div ng-show="!$ctrl.err" class="alert alert alert-success center-block">
            <label style="font-weight: normal; margin-bottom: 0; cursor: pointer;">
                <input type="checkbox" ng-model="$ctrl.isFlattrOn" ng-click="$ctrl.toggleFlattr()">
                &nbsp;&nbsp;Perform a daily <strong>Flattr</strong>
            </label>
        </div>
        <div ng-repeat="podcast in $ctrl.podcasts" class="col-sm-6 col-md-3">
            <div class="thumbnail">
                <a ng-href="{{podcast.trackViewUrl}}" target="_blank">
                    <img ng-src="{{podcast.artworkUrl600}}" class="img-responsive">
                </a>
                <div class="caption">
                    <a ng-href="{{podcast.trackViewUrl}}" target="_blank">
                        <h3 class="truncate" title="{{podcast.trackName}}" style="max-width: 300px;">{{podcast.trackName}}</h3>
                    </a>
                    <a ng-href="{{podcast.artistViewUrl}}" target="_blank">
                        <p class="truncate" title="{{podcast.artistName}}"><small>By:</small> {{podcast.artistName}}</p>
                    </a>
                    <a ng-href="{{podcast.collectionViewUrl}}" target="_blank">
                        <p class="truncate" title="{{podcast.collectionName}}">{{podcast.collectionName}}</p>
                    </a>
                    <br>
                    <button ng-click="$ctrl.removePodcast(podcast.trackId)" class="btn btn-success btn-sm btn-block">
                        Un-<strong>Flattr</strong>
                    </a>
                </div>
            </div>
        </div>
    `,
    controller: function ($http) {
        var me = this;

        me.getPodcasts = function () {
            $http.get('/flattr/get-podcasts')
                .then(function (res) {
                    me.podcasts = res.data.podcasts;
                    me.isFlattrOn = res.data.isFlattrOn
                }, function (err) {
                    me.err = err;
                });
        };

        me.removePodcast = function (podcastId) {
            if (confirm('Remove podcast?')) {
                $http.post('/flattr/remove-podcast', {podcastId: podcastId})
                    .then(function (res) {
                        me.getPodcasts();
                        alert(res.data);
                    }, function (err) {
                        me.err = err;
                    });
            }
        };

        me.toggleFlattr = function () {
            $http.post('/flattr/toggle-flattr', {isFlattrOn: me.isFlattrOn})
                .then(function (res) {
                    alert(res.data);
                }, function (err) {
                    me.err = err;
                });
        };

        me.getPodcasts();
    }
});
