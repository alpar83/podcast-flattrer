const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();

const onError = err => {
    return res.status(err.code).send(err.message);
};

// user model
const User = mongoose.model('User', {
    token: String,
    podcasts: Array,
    isFlattrOn: {type: Boolean, default: true}
});

// find users podcasts if user exists
const getUser = (accessToken) => {
    return User.findOne({'token': accessToken});
};

// create new user
const addUser = (accessToken) => {
    const user = new User({token: accessToken});

    return user.save();
};

// add podcast if it doesn't exist yet
const addPodcast = (user, podcast) => {
    return new Promise((resolve, reject) => {
        for (i of user.podcasts) {
            if (i.trackId === podcast.trackId) {
                return resolve('Already added');
            }
        }

        user.podcasts.push(podcast);
        user.save(err => {
            if (err) reject(err);

            resolve('Podcast added');
        });
    });
};

// remove podcast by id
const removePodcast = (user, podcastId) => {
    return new Promise((resolve, reject) => {
        for (let i in user.podcasts) {
            if (user.podcasts[i].trackId === podcastId) {
                user.podcasts.splice(i, 1);
                user.save(err => {
                    if (err) reject(err);

                    resolve('Podcast removed');
                });

                return;
            }
        }

        resolve('Podcast not found');
    });
};

// add podcast route
router.post('/add-podcast', (req, res) => {
    if (!req.cookies.podcast_flattrer_auth) {
        return res.status(403).send('Please sign in!');
    }

    const accessToken = req.cookies.podcast_flattrer_auth;
    const podcast = req.body.podcast;

    (async _ => {
        let user = await getUser(accessToken);
        if (!user) user = await addUser(accessToken);
        let added = await addPodcast(user, podcast);

        res.status(200).send(added);
    })();
});

// remove podcast route
router.post('/remove-podcast', (req, res) => {
    if (!req.cookies.podcast_flattrer_auth) {
        return res.status(403).send('Please sign in!');
    }

    const accessToken = req.cookies.podcast_flattrer_auth;
    const podcastId = req.body.podcastId;

    (async _ => {
        let user = await getUser(accessToken);
        let removed = await removePodcast(user, podcastId);

        res.status(200).send(removed);
    })();
});

// get flattred podcasts route
router.get('/get-podcasts', (req, res) => {
    if (!req.cookies.podcast_flattrer_auth) {
        return res.status(403).send('Please sign in!');
    }

    const accessToken = req.cookies.podcast_flattrer_auth;

    (async _ => {
        let user = await getUser(accessToken);

        res.status(200).json({
            podcasts: user ? user.podcasts : [],
            isFlattrOn: user.isFlattrOn
        });
    })();
});

// toggle flattring route
router.post('/toggle-flattr', (req, res) => {
    if (!req.cookies.podcast_flattrer_auth) {
        return res.status(403).send('Please sign in!');
    }

    const accessToken = req.cookies.podcast_flattrer_auth;

    (async _ => {
        let user = await getUser(accessToken);
        user.isFlattrOn = req.body.isFlattrOn;
        let saved = await user.save();
        const mess = saved.isFlattrOn ? 'ON' : 'OFF';

        res.status(200).send('Daily Flattrs ' + mess);
    })();
});

module.exports = router;
