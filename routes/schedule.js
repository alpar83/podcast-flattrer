const schedule = require('node-schedule');
const mongoose = require('mongoose');
const Parser = require('rss-parser');
const request = require('request');
const config = require('../config/flattr');

// get all users
const getUsers = _ => {
    return new Promise((resolve, reject) => {
        mongoose.model('User').find({}, (err, users) => {
            if (err) reject(err);

            resolve(users);
        });
    });
};

// get rss feed
const getFeed = feedUrl => {
    return new Parser().parseURL(feedUrl);
};

// submit podcast url to the flattr api
const submitPodcast = (accessToken, podcastUrl) => {
    return new Promise((resolve, reject) => {
        request.post({
            url: config.submitUrl,
            headers: {
                'Authorization': accessToken,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({url: podcastUrl})
        }, (err, res, body) => {
            if (err) reject(err);

            resolve(JSON.parse(body));
        });
    });
};

const flattr = async () => {
    // get all users
    let users = await getUsers();

    // for each user...
    for (user of users) {

        // if flattr is on
        if (user.podcasts.length > 0 && user.isFlattrOn) {

            // for each podcast...
            for (podcast of user.podcasts) {
                // get the rss feed
                let feed = await getFeed(podcast.feedUrl);

                // if new episode
                if (!podcast.lastFlattred || feed.items.length > podcast.lastFlattred) {
                    if (!feed.items[0].link) continue;

                    // submit the episode url
                    let submit = await submitPodcast(user.token, feed.items[0].link);

                    console.log('USER', user.token);
                    console.log('TRACK', feed.items[0].title);
                    console.log('SUBMIT', submit);
                    console.log('');

                    if (!submit.error) {
                        // update podcast's last flattred flag
                        podcast.lastFlattred = feed.items.length;

                        let save = await user.save();

                        console.log('DONE', save);
                    }
                }
            }
        }
    }
};

// flattr podcasts once a day
(_ => {
    console.log('Schedule started');

    schedule.scheduleJob('0 1 * * * *', _ => {
    // schedule.scheduleJob('1 * * * * *', () => {
        console.log('Flattr ', new Date());

        flattr();
    });
})();
