const express = require('express');
const request = require('request');
const config = require('../config/flattr');
const router = express.Router();

// get access token from the flattr api
const getAccessToken = (authHeader, authCode) => {
    return new Promise((resolve, reject) => {
        request.post({
            url: config.tokenUrl,
            headers: {
                'Authorization': authHeader,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'grant_type': 'authorization_code',
                'code': authCode
            })
        }, (err, res, body) => {
            if (err) reject(err);

            resolve(JSON.parse(body).token_type + ' ' + JSON.parse(body).access_token);
        });
    });
};

// auth route
router.get('/', (req, res) => {
    if (!req.query.code) {
        return res.status(400).send('Authorization code is missing');
    }

    const authHeader = 'Basic ' + new Buffer(config.clientKey + ':' + config.clientSecret).toString('base64');
    const authCode = req.query.code;

    getAccessToken(authHeader, authCode)
        .then(accessToken => {
            res.cookie('podcast_flattrer_auth', accessToken, {expires: 0, httpOnly: true});
            res.redirect('/#' + req.query.from);
        }, err => {
            res.status(err.code).send(err.message);
        });
});

router.get('/check-login', (req, res) => {
    res.status(200).send(!!req.cookies.podcast_flattrer_auth);
});

router.get('/sign-out', (req, res) => {
    res.clearCookie('podcast_flattrer_auth');
    res.status(200).send('Signed out');
});

module.exports = router;
